package utils;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

public class MysqlCon {
	static Connection con = null;
	private static String jdbc;
	private static String servidor;
	private static String usuario;
	private static String password;
	public static void main(String args[]) {
		try {
			getConfiguration();
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(jdbc+servidor, usuario, password);
			// here sonoo is database name, root is username and password
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from publicaciones");
			while (rs.next())
				System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}


	public static void getConfiguration() {
		String configuracion = "ConfigBD.txt";
		Properties propiedades;

		// Carga del fichero de propiedades
		try {
			FileInputStream f = new FileInputStream(configuracion);
			propiedades = new Properties();
			propiedades.load(f);
			f.close();

			// Leo los valores de configuracion
			jdbc = propiedades.getProperty("jdbc");
			servidor = propiedades.getProperty("servidor");
			usuario = propiedades.getProperty("usuario");
			password = propiedades.getProperty("password");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setConnection() throws ClassNotFoundException, SQLException {
		if (con!=null) {
			if (con.isClosed()) {
				getConfiguration();
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection(jdbc+servidor, usuario, password);
			} 
		}else {
			getConfiguration();
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(jdbc+servidor, usuario, password);
		}
	}

	public static PreparedStatement getQuery(String sql) throws SQLException {
		return con.prepareStatement(sql);
	}

	public static ResultSet executeQuery(PreparedStatement query) throws SQLException {
		return query.executeQuery();
	}

//	public static void endConnecction() throws SQLException {
//		con.close();
//	}
}