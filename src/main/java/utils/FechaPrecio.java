package utils;

import java.sql.Date;

@SuppressWarnings("hiding")
public class FechaPrecio<LocalDate,Double>{
		private Double precio;
		private Date fecha;
		
		public FechaPrecio(Date fecha,Double precio) {
			super();
			this.precio = precio;
			this.fecha = fecha;
		}
		public Double getPrecio() {
			return precio;
		}
		public Date getFecha() {
			return fecha;
		}
		public void setPrecio(Double precio) {
			this.precio = precio;
		}
		public void setFecha(Date fecha) {
			this.fecha = fecha;
		}
}