package utils;

import java.sql.Date;
import java.util.Calendar;

public final class DateUtils {

	public static final Date getDateSinHora(Date fecha) {
		Calendar date = Calendar.getInstance();
		date.setTime(fecha);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		return new Date(date.getTimeInMillis());
	}

	public static final Date getFechaSql(java.util.Date fecha) {
		Calendar date = Calendar.getInstance();
		date.setTime(fecha);
		return new Date(date.getTimeInMillis());
	}

	public static final Date getDateConHora(int anio, int mes, int dia, int hora, int min) {
		Calendar date = Calendar.getInstance();
		date.setTime(new java.util.Date());
		date.set(anio, mes - 1, dia, hora, min);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		return new Date(date.getTimeInMillis());
	}

	public static final int get(Date fecha, int calendarInt) {
		Calendar date = Calendar.getInstance();
		date.setTime(fecha);
		return date.get(calendarInt);
	}

//	public static final Date getDateSql(java.util.Date fecha) {		
//		return (Date) fecha;
//	}
	public static String getHoraString(Date fechaYHora) {
		Calendar date = Calendar.getInstance();
		date.setTime(fechaYHora);
		return date.get(Calendar.HOUR_OF_DAY) + ":" + date.get(Calendar.MINUTE);
	}

	public static final Date getDateSinHora(Date fecha, int days) {
		Calendar date = Calendar.getInstance();
		date.setTime(fecha);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		date.add(Calendar.DATE, -days);
		return new Date(date.getTimeInMillis());
	}

	public static Date parseDMY(String fecha) {
		if ((fecha == null ? false : !fecha.equalsIgnoreCase("null"))) {
			return getDateConHora(Integer.parseInt(fecha.substring(6)), Integer.parseInt(fecha.substring(3, 5)),
					Integer.parseInt(fecha.substring(0, 2)), 0, 0);
		}
		return null;
	}

	public static Date parseYMD(String fecha) {
		return getDateConHora(Integer.parseInt(fecha.substring(0, 4)), Integer.parseInt(fecha.substring(5, 7)),
				Integer.parseInt(fecha.substring(8)), 0, 0);
	}

}
