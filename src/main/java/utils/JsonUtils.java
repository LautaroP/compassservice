package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DAO.EstadisticasDAO;
import clasessistema.Estadistica;

public class JsonUtils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private static JSONObject valoresPorDias;
	private static JSONObject jsonObject;
	private static final String API_KEY = "VVR6DJJDGL9JWM90";

	private static Iterator<String> iteratorFechas;

	private static JSONObject valoresPorMes;

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
//			jsonText.replace(']', ' ');
//			jsonText.replace('[', ' ');
			return new JSONObject(jsonText);
		} finally {
			is.close();
		}
	}

	public static JSONArray readJsonArrayFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
//			jsonText.replace(']', ' ');
//			jsonText.replace('[', ' ');
			return new JSONArray(jsonText);
		} finally {
			is.close();
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws JSONException
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args)
			throws IOException, JSONException, ClassNotFoundException, InterruptedException, SQLException {
		try {
			BufferedReader bufferRead;
			do {
				String seleccion;
				do {
					System.out.println(
							"Seleccione una operación:\n1-Actualizar Todos\n2-Agregar valores\n3-Actualizar un valor\n0-salir\n ");
					bufferRead = new BufferedReader(new InputStreamReader(System.in));
					seleccion = bufferRead.readLine();
				} while (seleccion.isEmpty());
				try {
					switch (seleccion) {
					case "1":
						actualizarValores();
						System.out.println("Se actualizaron los valores\n");
						break;
					case "2":
						System.out.println("\nIngrese el symbolo de la Moneda:\n ");
						bufferRead = new BufferedReader(new InputStreamReader(System.in));
						seleccion = bufferRead.readLine();
						List<String> monedas;
						if (!seleccion.isEmpty()) {
							monedas = insert(seleccion);

							System.out.println("Se agregaron:\n");
							for (String moneda : monedas) {
								System.out.println(moneda);
							}
						}
						System.out.println("No se ingreso ningun valor");
						break;
					case "3":
						System.out.println("\nIngrese el Symbolo de la Moneda:\n ");
						bufferRead = new BufferedReader(new InputStreamReader(System.in));
						seleccion = bufferRead.readLine();
						if (!seleccion.isEmpty()) {
							Estadistica estadistica = new Estadistica();
							estadistica.setSymbol(seleccion);
							estadistica = EstadisticasDAO.getEstadistica(estadistica);
							if (estadistica.getId() != null) {
								actualizarValor(estadistica);
							} else {
								System.out.println("Valor no Encontrado");
							}
						} else {
							System.out.println("No se ingreso ningun valor");
						}
						break;
					case "0":
						return;
					default:
						break;
					}

				} catch (NumberFormatException e) {
					System.out.println("\nseleccion Incorrecta, vuelva a intentar");
				}
			} while (true);
		} catch (IOException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
//}
//		insert("m");
//		JSONArray json = readJsonArrayFromUrl("http://localhost:8080/estadisticas/2");
//		System.out.println(json);
	}

	private static void actualizarValor(Estadistica estadistica) {
		getJsonValues(estadistica);
		if (!jsonObject.keySet().contains("Error Message")) {
			System.out.println(valoresPorDias);
			iteratorFechas = valoresPorDias.keys();
			updateEstadisticas(estadistica);

		}
	}

	private static void actualizarValores() {

		List<Estadistica> estadisticas;
		try {
			estadisticas = EstadisticasDAO.getAllEstadisticas(Integer.MAX_VALUE);

			for (Estadistica estadistica : estadisticas) {
				actualizarValor(estadistica);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
//			EstadisticasDAO.update(estadistica);
	}

	private static void getJsonValues(Estadistica estadistica) {
		valoresPorDias=null;
		valoresPorMes=null;
		while (true) {
			try {

				jsonObject = readJsonFromUrl(getValues(estadistica.getSymbol()));
				if (!jsonObject.keySet().contains("Note")) {
					if (!jsonObject.keySet().contains("Error Message"))
						valoresPorDias = jsonObject.getJSONObject("Time Series (Daily)");
					break;
				} else {
					System.out.println(jsonObject.getString("Note"));
					Thread.sleep(60100);
				}
			} catch (Exception e) {
				e.printStackTrace();
				//MysqlCon.endConnecction();
			}
		}
		while (true) {
			try {

				JSONObject joPorMes = readJsonFromUrl(getValuesMes(estadistica.getSymbol()));
				if (!joPorMes.keySet().contains("Note")) {
					if (!joPorMes.keySet().contains("Error Message"))
						valoresPorMes = joPorMes.getJSONObject("Monthly Adjusted Time Series");
					break;
				} else {
					System.out.println(joPorMes.getString("Note"));
					Thread.sleep(60100);
				}
			} catch (Exception e) {
				e.printStackTrace();
				//MysqlCon.endConnecction();
			}
		}

	}

	private static String getValuesMes(String symbol) {
		return "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol=" + symbol
				+ "&outputsize=compact&apikey=" + API_KEY;
	}

	public static List<String> insert(String symbol)
			throws JSONException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		JSONObject json = readJsonFromUrl(
				"https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + symbol + "&apikey=" + API_KEY);
		JSONArray monedas = json.getJSONArray("bestMatches");
		System.out.println(monedas);
		JSONObject rec = monedas.getJSONObject(monedas.length() - 1);
		List<String> monedasAgregadas = new ArrayList<>();
		for (int j = 0; j < monedas.length() - 1; j++) {
			Estadistica estadistica = new Estadistica();
			rec = monedas.getJSONObject(j);
			estadistica.setSymbol(rec.getString("1. symbol"));
			estadistica.setNombreAccionMoneda(rec.getString("2. name"));
			if (EstadisticasDAO.getEstadistica(estadistica).getId() == null) {
				getJsonValues(estadistica);
				if (!jsonObject.keySet().contains("Error Message")) {
					monedasAgregadas.add(rec.getString("1. symbol"));
					System.out.println(valoresPorDias);
					iteratorFechas = valoresPorDias.keys();
					try {
						Date fecha = DateUtils.parseYMD(iteratorFechas.next());
						Double precio = valoresPorDias.getJSONObject(sdf.format(fecha)).getDouble("4. close");
						estadistica.setPrecioActual(new FechaPrecio<Date, Double>(fecha, precio));
						EstadisticasDAO.insert(estadistica);
					} catch (SQLException | ClassNotFoundException e1) {
						e1.printStackTrace();
					}
					updateEstadisticas(estadistica);
				}

//			EstadisticasDAO.update(estadistica);
			}
		}
		return monedasAgregadas;
	}

	private static void updateEstadisticas(Estadistica estadistica) {
		if (estadistica.getId()==null) {
			try {
				estadistica = EstadisticasDAO.getEstadistica(estadistica);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
		while (iteratorFechas.hasNext()) {
			Date fecha = null;
			try {
				fecha = DateUtils.parseYMD(iteratorFechas.next());
				Double precio = valoresPorDias.getJSONObject(sdf.format(fecha)).getDouble("4. close");
				estadistica.setPrecioActual(new FechaPrecio<Date, Double>(fecha, precio));
				EstadisticasDAO.update(estadistica);
			} catch (JSONException e) {
				System.out.println(fecha + e.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println(fecha + e.getMessage());
			} catch (SQLException e) {
				System.out.println(fecha + e.getMessage());
			}
		}
		Iterator<String> iterator = valoresPorMes.keys();
		while (iterator.hasNext()) {
			Date fecha = null;
			try {
				fecha = DateUtils.parseYMD(iterator.next());
				Double precio = valoresPorMes.getJSONObject(sdf.format(fecha)).getDouble("4. close");
				estadistica.setPrecioActual(new FechaPrecio<Date, Double>(fecha, precio));
				EstadisticasDAO.update(estadistica);
			} catch (JSONException e) {
				System.out.println(fecha + e.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println(fecha + e.getMessage());
			} catch (SQLException e) {
				System.out.println(fecha + e.getMessage());
			}
		}

	}

	private static String getValues(String filter) {
		return "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + filter
				+ "&outputsize=compact&apikey=" + API_KEY;
	}

}