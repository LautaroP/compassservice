package API;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DAO.UsuariosDAO;
import clasessistema.Usuario;
import utils.MysqlCon;

@RestController
@RequestMapping("/usuarios")
public class UsuarioSpringController {

	private static final String USER_NAME = "nombreUsuario={nombreUsuario}";
	private static final String MAIL = "mail={mail}";
	private static final String PASSWORD = "password={password}";
	private static final String ID_USUARIO = "usuario={idUsuario}";
	private static final String CONFIRMADO = "confirmado={confirmado}";
	private static final String DESCRIPCION = "descripcion={descripcion}";
	private static final String FONDOS = "fondos={fondos}";

	@GetMapping("/login/" + USER_NAME + "&" + PASSWORD)
	Usuario loginByNombreUsuario(@PathVariable String nombreUsuario, @PathVariable String password) {
		try {
			Usuario usuario = new Usuario();
			usuario.setNombreUsuario(nombreUsuario);
			usuario.setContrasenia(password);
			return UsuariosDAO.selectLogin(usuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
		}
		return new Usuario();
	}

	@GetMapping("/login/" + MAIL + "&" + PASSWORD)
	Usuario loginByMail(@PathVariable String mail, @PathVariable String password) {
		try {
			Usuario usuario = new Usuario();
			usuario.setMail(mail);
			usuario.setContrasenia(password);
			return UsuariosDAO.selectLogin(usuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
			return null;
		}
	}

	@GetMapping("/singin/" + USER_NAME + "&" + PASSWORD + "&" + MAIL + "&" + DESCRIPCION)
	boolean singIn(@PathVariable String nombreUsuario, @PathVariable String password, @PathVariable String mail,@PathVariable String descripcion) {
		try {
			Usuario usuario = new Usuario();
			usuario.setMail(mail);
			usuario.setContrasenia(password);
			usuario.setNombreUsuario(nombreUsuario);
			usuario.setDescripcion(descripcion);
			UsuariosDAO.insert(usuario);
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
			return false;
		}
	}
	@GetMapping("/all")
	List<Usuario> getAll() {
		try {
			return UsuariosDAO.selectAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@GetMapping("/getSome/" + USER_NAME)
	List<Usuario> getSome(@PathVariable String nombreUsuario) {
		try {
			return UsuariosDAO.selectSame(nombreUsuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/get/" + USER_NAME)
	Usuario getUsuarioByUserName(String nombreUsuario) {
		Usuario usuario = new Usuario();
		usuario.setNombreUsuario(nombreUsuario);
		try {
			return UsuariosDAO.selectUsuarioNoMontos(usuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
		}
		return null;
	}

	@GetMapping("/get/" + MAIL)
	Usuario getUsuarioByMail(String mail) {
		Usuario usuario = new Usuario();
		usuario.setMail(mail);
		try {
			return UsuariosDAO.selectUsuarioNoMontos(usuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
		}
		return null;
	}

	@GetMapping("/update/" + ID_USUARIO + "&" + USER_NAME + "&" + PASSWORD + "&" + CONFIRMADO + "&" + DESCRIPCION + "&"
			+ FONDOS+"&"+MAIL)
	boolean updateUsuario(@PathVariable int idUsuario, @PathVariable String nombreUsuario,
			@PathVariable String password, @PathVariable boolean confirmado, @PathVariable String descripcion,
			@PathVariable double fondos,@PathVariable String mail) {
		Usuario usuario = new Usuario(idUsuario);
		usuario.setContrasenia(password);
		usuario.setNombreUsuario(nombreUsuario);
		usuario.setConfirmado(confirmado);
		usuario.setDescripcion(descripcion);
		usuario.setFondosDisponibles(fondos);
		usuario.setMail(mail);
		try {
			UsuariosDAO.update(usuario);
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
			return false;
		}
	}

}
