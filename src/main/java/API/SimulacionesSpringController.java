package API;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DAO.MontoDAO;
import DAO.SimulacionDAO;
import clasessistema.Estadistica;
import clasessistema.Monto;
import clasessistema.Simulacion;
import clasessistema.Usuario;

@RestController
@RequestMapping("/simulaciones")
public class SimulacionesSpringController {

	private static final String ID_ESTADISTICA = "symbol={symbol}";
	private static final String ID_USUARIO = "usuario={idUsuario}";
	private static final String MONTO = "monto={montoId}";
	private static final String ID_SIMULACION = "simulacion={idSimulacion}";
	private static final String LIMIT = "limit={limit}";
	private static final String FONDOS = "fondos={fondos}";
	private static final String SYMBOL = "symbol={symbol}";
	private static final String CANTIDAD = "cantidad={cantidad}";
	private static final String PRECIO_INICIAL = "precio={precioInicio}";

	@GetMapping("/simular/" + SYMBOL + "&" + ID_USUARIO + "&" + MONTO + "&" + CANTIDAD + "&" + PRECIO_INICIAL)
	Monto insert(@PathVariable String symbol, @PathVariable long idUsuario, @PathVariable int montoId , @PathVariable double cantidad,@PathVariable double precioInicio) {
		try {
			SimulacionDAO.insert(
					new Simulacion(new Estadistica(symbol), new Usuario(idUsuario), new Monto(montoId), null, null,precioInicio));
			return MontoDAO.calularDevolucion(montoId,cantidad);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/end/" + ID_SIMULACION)
	boolean end(@PathVariable int idSimulacion) {
		try {
			SimulacionDAO.endSimulation(new Simulacion(idSimulacion, null, null));
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
			return false;
		}
	}

	@GetMapping("/get/" + LIMIT + "&" + ID_USUARIO)
	List<Simulacion> selectSimulacion(@PathVariable long idUsuario, @PathVariable int limit) {
		try {
			List<Simulacion> simulacion = SimulacionDAO
					.selectSimulaciones(new Simulacion(null, new Usuario(idUsuario), new Monto(), null, null,0), limit);
			return simulacion;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			// MysqlCon.endConnecction();
			return null;
		}
	}

	@GetMapping("/simularYCrearMonto/" + SYMBOL + "&" + ID_USUARIO + "&" + FONDOS+"&" + PRECIO_INICIAL)
	boolean crearMontoYSimular(@PathVariable String symbol, @PathVariable long idUsuario, @PathVariable double fondos,@PathVariable double precioInicio) {
		try {
			SimulacionDAO.insert(
					new Simulacion(new Estadistica(symbol), new Usuario(idUsuario), MontoDAO.insert(new Monto(new Usuario(idUsuario), fondos)), null, null,precioInicio));
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			return false;
		}
	}
}
