package API;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DAO.PublicacionesDAO;
import clasessistema.Publicacion;
import clasessistema.Usuario;
import utils.MysqlCon;

@RestController
@RequestMapping("/publicaciones")
public class PublicacionSpringController {

	private static final String LIKE = "like={like}";
	private static final String ID_USUARIO = "usuario={idUsuario}";
	private static final String LIMIT = "limit={limit}";
	private static final String COMENTARIO = "comentario={comentario}";
	private static final String ID_PUBLICACION = "publicacion={idPublicacion}";
	private static final String TITULO= "titulo={titulo}";


	@GetMapping("/publicar/" + ID_USUARIO + "&" + COMENTARIO + "&" + TITULO)
	boolean publicar(@PathVariable int idUsuario, @PathVariable String comentario,@PathVariable String titulo) {
		try {
			Publicacion publicacion = new Publicacion(comentario, new Usuario(idUsuario),titulo);
			PublicacionesDAO.insert(publicacion);
			return true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();

		} catch (SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return false;
	}

	@GetMapping("/like/" + LIKE + "&" + ID_PUBLICACION)
	boolean likeDislike(@PathVariable int idPublicacion, @PathVariable boolean like) {
		try {
			PublicacionesDAO.likeDislike(idPublicacion, like);
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return false;
	}

	@GetMapping("/all/" + LIMIT + "")
	List<Publicacion> getPublicaciones(@PathVariable int limit) {
		try {
			return PublicacionesDAO.selectPublicaciones(limit);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return new ArrayList<>();
	}

	@GetMapping("/all/" + LIMIT + "&" + ID_USUARIO)
	public static List<Publicacion> selectComentariosUsuario(@PathVariable int limit, @PathVariable long idUsuario) {
		try {
			return PublicacionesDAO.selectPublicacionesByUsuario(limit, idUsuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return new ArrayList<>();
	}

//	@GetMapping("/getOne/user={idUsuario}&content={comentario}")
//	Publicacion getPublicacion(@PathVariable int idUsuario, @PathVariable String comentario) {
//		try {
//			Publicacion publicacion = new Publicacion(comentario, new Usuario());
//			publicacion.getCreador().setId(idUsuario);
//			PublicacionesDAO.insert(publicacion);
//			return true;
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace(); //MysqlCon.endConnecction();
//
//		} catch (SQLException e) {
//			e.printStackTrace(); //MysqlCon.endConnecction();
//		}
//		return false;
//	}

}
