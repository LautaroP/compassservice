package API;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DAO.OpinionDAO;
import clasessistema.Opinion;
import clasessistema.Publicacion;
import clasessistema.Usuario;
import utils.MysqlCon;

@RestController
@RequestMapping("/opiniones")
public class OpinionesSpringController {

	private static final String ID_PUBLICACION = "publicacion={idPublicacion}";
	private static final String ID_USUARIO = "usuario={idUsuario}";
	private static final String COMENTARIO = "comentario={comentario}";
	private static final String LIKE = "like={like}";

	@GetMapping("opinar/" + ID_PUBLICACION + "&" + ID_USUARIO + "&" + COMENTARIO)
	Opinion opinar(@PathVariable long idPublicacion, @PathVariable long idUsuario, @PathVariable String comentario) {
		Opinion opinion = new Opinion(comentario, new Usuario(idUsuario));
		opinion.setPublicacion(new Publicacion(idPublicacion));
		try {
			return OpinionDAO.insert(opinion);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return null;
	}

//	public static void delete(long idOpinion)  {
//		MysqlCon.setConnection();
//		PreparedStatement query = MysqlCon.getQuery(DELETE_OPINION);
//		query.setLong(1, idOpinion);
//		query.execute();
//		//MysqlCon.endConnecction();
//	}

	@GetMapping("/like/" + LIKE + "&" + ID_PUBLICACION)
	boolean likeDislike(@PathVariable int idPublicacion, @PathVariable boolean like) {
		try {
			OpinionDAO.likeDislike(idPublicacion, like);
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return false;
	}

	@GetMapping("/from/" + ID_PUBLICACION + "")
	List<Opinion> getComentariosByPublicacion(@PathVariable long idPublicacion) {
		try {
			return OpinionDAO.selectComentariosPublicacion(idPublicacion);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return new ArrayList<>();
	}

	@GetMapping("/from/" + ID_USUARIO)
	List<Opinion> getComentariosByUsuario(@PathVariable long idUsuario) {
		try {
			return OpinionDAO.selectComentariosUsuario(idUsuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
		}
		return new ArrayList<>();
	}

}
