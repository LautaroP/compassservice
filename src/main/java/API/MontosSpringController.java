package API;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DAO.MontoDAO;
import clasessistema.Monto;
import clasessistema.Usuario;
import utils.MysqlCon;

@RestController
@RequestMapping("/montos")
public class MontosSpringController {
	private static final String ID_USUARIO = "usuario={idUsuario}";
	private static final String FONDOS = "fondos={fondos}";
	private static final String ID_MONTO = "idMonto={idMonto}";

	@GetMapping("/crear/" + ID_USUARIO + "&" + FONDOS)
	Monto insert(@PathVariable long idUsuario, @PathVariable double fondos) {
		try {
			
			return MontoDAO.insert(new Monto(new Usuario(idUsuario), fondos));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return null;
		}
	}

//	public static void delete(int idMonto)  {
//		MysqlCon.setConnection();
//		PreparedStatement query = MysqlCon.getQuery(DELETE_MONTO);
//		query.setInt(1, idMonto);
//		query.execute();
//		//MysqlCon.endConnecction();
//	}
	@GetMapping("/prestar/" + ID_USUARIO + "&" + FONDOS + "&" + ID_MONTO)
	boolean update(@PathVariable double fondos, @PathVariable long idUsuario, @PathVariable int idMonto) {
		try {
			MontoDAO.update(new Monto(idMonto, fondos), idUsuario);
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return false;
		}
	}

	@GetMapping("/getYours/" + ID_USUARIO)
	List<Monto> selectMotosPoseidos(@PathVariable long idUsuario) {
		try {
			return MontoDAO.selectMotosPoseidos(idUsuario);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new ArrayList<>();
		}
	}

}
