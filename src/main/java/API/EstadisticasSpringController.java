package API;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DAO.EstadisticasDAO;
import clasessistema.Estadistica;

@RestController
@RequestMapping("/estadisticas")
public class EstadisticasSpringController {

	private static final String LIMIT = "limit={limit}";
	private static final String SYMBOL = "symbol={symbol}";

	@GetMapping("/all/" + LIMIT)
	List<Estadistica> all(@PathVariable int limit) {
		try {
			return EstadisticasDAO.getAllEstadisticas(limit);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new ArrayList<>();
		} catch (SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new ArrayList<>();
		}
	}

	@GetMapping("/id={id}")
	Estadistica estadisticaById(@PathVariable int id) {
		try {
			Estadistica estadistica = new Estadistica();
			estadistica.setId(id);
			return EstadisticasDAO.getEstadistica(estadistica);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new Estadistica();
		} catch (SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new Estadistica();
		}
	}

	@GetMapping("/" + SYMBOL)
	Estadistica estadisticaBySymbol(@PathVariable String symbol) {
		try {
			Estadistica estadistica = new Estadistica();
			estadistica.setSymbol(symbol);
			;
			return EstadisticasDAO.getEstadistica(estadistica);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new Estadistica();
		} catch (SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new Estadistica();
		}
	}

	@GetMapping("/anteriores/" + SYMBOL)
	Estadistica estadisticaAnteriores(@PathVariable String symbol) {
		try {
			Estadistica estadistica = new Estadistica();
			estadistica.setSymbol(symbol);
			return EstadisticasDAO.getEstadisticasAnteriores(estadistica);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new Estadistica();
		} catch (SQLException e) {
			e.printStackTrace();
			//MysqlCon.endConnecction();
			return new Estadistica();
		}
	}

//	@PostMapping("/Est")
//	Estadistica newEstadistica(@RequestBody Estadistica newEstadistica) {
////		return repository.save(newEstadistica);
//		return null;
//	}
}
