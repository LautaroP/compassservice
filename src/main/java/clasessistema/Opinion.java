package clasessistema;

import java.util.Date;

public class Opinion {
	private String comentario;
	private int likeDislike;
	private Date fechaPublicaion;
	private Usuario creador;
	private long id;
	private Publicacion publicacion;

	public long getId() {
		return id;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public void setLikeDislike(int likeDislike) {
		this.likeDislike = likeDislike;
	}

	public void setFechaPublicaion(Date fechaPublicaion) {
		this.fechaPublicaion = fechaPublicaion;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Opinion(String comentario, Usuario creador) {
		super();
		this.comentario = comentario;
		this.creador = creador;
		this.likeDislike = 0;
	}

	public String getComentario() {
		return comentario;
	}

	public Date getFechaPublicaion() {
		return fechaPublicaion;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void likeDislike(boolean like) {
		if (like) {
			likeDislike++;
		} else {
			likeDislike--;
		}
	}

	public int getLikeDislike() {
		return likeDislike;
	}

	public Publicacion getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}
}
