package clasessistema;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Usuario {
	private long id;
	private String nombreUsuario = null;
//	private Date fechaNacimiento;
	private String contrasenia = null;
	private double fondosDisponibles;
	private String descripcion;
	private String mail = null;
	private boolean confirmado;
//	private List<Usuario> subscrividos; 
	private List<Monto> montos = new ArrayList<>();

	public Usuario(long id) {
		super();
		this.id = id;
	}

	public Usuario() {
		super();
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public double getFondosDisponibles() {
		return fondosDisponibles;
	}

	public String getMail() {
		return mail;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public List<Monto> getMontos() {
		return montos;
	}

	public void addMonto(Monto monto) {
		montos.add(monto);
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public Monto createMonto(double cantidad) {
		Monto monto = new Monto(this, cantidad);
		montos.add(monto);
		return monto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public void setFondosDisponibles(double fondosDisponibles) {
		this.fondosDisponibles = fondosDisponibles;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isConfirmado() {
		return confirmado;
	}

	public void setConfirmado(boolean confirmado) {
		this.confirmado = confirmado;
	}

	public void setMontos(List<Monto> montos) {
		this.montos = montos;
	}
}
