package clasessistema;

public class Monto {
	private Usuario creador;
	private double cantidad;
	private Integer id;
	private Usuario posedor;
	private boolean invertido = false;

	public Monto(Usuario creador, double cantidad) {
		super();
		this.creador = creador;
		this.cantidad = cantidad;
		this.posedor = creador;
	}

	public boolean isInvertido() {
		return invertido;
	}

	public void setInvertido(boolean invertido) {
		this.invertido = invertido;
	}

	public Monto(Usuario creador, double cantidad, Usuario posedor) {
		super();
		this.creador = creador;
		this.cantidad = cantidad;
		this.posedor = posedor;
	}

	public Monto(int id, double cantidad) {
		super();
		this.id = id;
		this.cantidad = cantidad;
	}

	public Monto(int id) {
		super();
		this.id = id;
	}

	public Monto() {
		super();
	}

	public Monto(Usuario creador, double cantidad, Usuario posedor, int id,boolean invertido) {
		super();
		this.creador = creador;
		this.cantidad = cantidad;
		this.posedor = posedor;
		this.id=id;
		this.invertido=invertido;
	}


	public double getCantidad() {
		return cantidad;
	}

	public void setMonto(double monto) {
		this.cantidad = monto;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getPosedor() {
		return posedor;
	}

	public void setPosedor(Usuario posedor) {
		this.posedor = posedor;
	}
}
