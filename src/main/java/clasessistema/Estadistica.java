package clasessistema;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import utils.FechaPrecio;

public class Estadistica {
	private String nombreAccionMoneda;
	private FechaPrecio<Date, Double> precioActual;
	private  Integer id = null;
	private String symbol;
	// se ingresan ordenados
	private List<FechaPrecio<Date, Double>> preciosAnteriores = new ArrayList<>();

	public Estadistica(int id) {
		super();
		this.id=id;
	}
	public Estadistica() {
		super();
	}

	public Estadistica(String symbol) {
		super();
		this.symbol=symbol;
	}
	public void setNombreAccionMoneda(String nombreAccionMoneda) {
		this.nombreAccionMoneda = nombreAccionMoneda;
	}

	public void setPreciosAnteriores(List<FechaPrecio<Date, Double>> preciosAnteriores) {
		this.preciosAnteriores = preciosAnteriores;
	}

	public FechaPrecio<Date, Double> getPrecioActual() {
		return precioActual;
	}

	public void setPrecioActual(FechaPrecio<Date, Double> precioActual) {
		if (precioActual!=null) {
			preciosAnteriores.add(this.precioActual);
		}
		this.precioActual = precioActual;
	}

	public void addPrecioAnterior(Date fecha, double precio) {
		preciosAnteriores.add(new FechaPrecio<Date, Double>(fecha, precio));
	}

	public String getNombreAccionMoneda() {
		return nombreAccionMoneda;
	}

	public List<FechaPrecio<Date, Double>> getPreciosAnteriores() {
		return preciosAnteriores;
	}

	public double getPrecioFecha(Date fecha) {
		for (FechaPrecio<Date, Double> fechaPrecio : preciosAnteriores) {
			if (fechaPrecio.getFecha().after(fecha)) {
				return fechaPrecio.getPrecio();
			}
		}
		return preciosAnteriores.get(preciosAnteriores.size() - 1).getPrecio();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
