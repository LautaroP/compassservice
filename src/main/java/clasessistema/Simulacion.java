package clasessistema;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Simulacion {
	private Estadistica estadisticas = null;
	private Usuario creador= null;
	private Monto monto= null;
	private Date fechaInicio= null;
	private Date fechaFin= null;
	private Integer id =null;
	private double precioInicio;

	public Integer getId() {
		return id;
	}

	public void setEstadisticas(Estadistica estadisticas) {
		this.estadisticas = estadisticas;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public void setMonto(Monto monto) {
		this.monto = monto;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Simulacion(Estadistica historiaMoneda, Usuario creador, Monto monto, Date fechaInicio, Date fechaFin, double precioInicio) {
		super();
		this.estadisticas = historiaMoneda;
		this.creador = creador;
		this.monto = monto;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.setPrecioInicio(precioInicio);
	}

	public Simulacion(Integer id, Date fechaInicio, Date fechaFin) {
		this.id=id;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Estadistica getEstadisticas() {
		return estadisticas;
	}

	public Usuario getCreador() {
		return creador;
	}

	public Monto getMonto() {
		return monto;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}
	@JsonIgnore
	public double getGanancias() {
		return monto.getCantidad() * (estadisticas.getPrecioActual().getPrecio() / precioInicio);
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getPrecioInicio() {
		return precioInicio;
	}

	public void setPrecioInicio(double precioInicio) {
		this.precioInicio = precioInicio;
	}

}
