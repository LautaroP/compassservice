package clasessistema;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Publicacion {
	private List<Opinion> opiniones = new ArrayList<>();
	private long id;
	private String comentario;
	private int likeDislike;
	private Date fechaPublicaion;
	private Usuario creador;
	private String titiulo;

	public void setOpiniones(List<Opinion> opiniones) {
		this.opiniones = opiniones;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public void setLikeDislike(int likeDislike) {
		this.likeDislike = likeDislike;
	}

	public void setFechaPublicaion(Date fechaPublicaion) {
		this.fechaPublicaion = fechaPublicaion;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Publicacion(String comentario, Usuario creador, String titulo) {
		super();
		this.comentario = comentario;
		this.creador = creador;
		this.opiniones = new ArrayList<>();
		this.likeDislike = 0;
		this.titiulo = titulo;
	}

	public Publicacion(long id) {
		this.id = id;
	}

	public String getComentario() {
		return comentario;
	}

	public Date getFechaPublicaion() {
		return fechaPublicaion;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void likeDislike(boolean like) {
		if (like) {
			likeDislike++;
		} else {
			likeDislike--;
		}
	}

	public int getLikeDislike() {
		return likeDislike;
	}

	public List<Opinion> getOpiniones() {
		return opiniones;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitiulo() {
		return titiulo;
	}

	public void setTitiulo(String titiulo) {
		this.titiulo = titiulo;
	}

}
