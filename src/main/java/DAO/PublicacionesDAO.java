package DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import clasessistema.Publicacion;
import clasessistema.Usuario;
import utils.MysqlCon;

public class PublicacionesDAO {
	private static final String ID_PUBLICACION = "idPublicaciones";

	private static final String COMENTARIO = "comentario";

	private static final String LIKE_DISLIKE = "likeDislike";

	private static final String PUBLICACIONES_TABLE = "publicaciones";

	private static final String ID_USUARIO = "idUsuario";

	private static final String FECHA_PUBLICACION = "fechaPublicacion";
	
	private static final String TITULO = "titulo";


	private static final String INSERT_PUBLICACION = "insert into " + PUBLICACIONES_TABLE + " (" + ID_PUBLICACION + ","
			+ COMENTARIO + ", " + FECHA_PUBLICACION + "," + ID_USUARIO + ","+TITULO+") values (?,?,CURRENT_TIMESTAMP,?,?)";
	private static final String UPDATE_LIKES = "update "+ PUBLICACIONES_TABLE + " set " + LIKE_DISLIKE + " = "+ LIKE_DISLIKE +" ? 1 where "+ID_PUBLICACION+" = ?";
	private static final String SELECT_PUBLICACIONES_GLOBAL = "select * from " + PUBLICACIONES_TABLE
			+ " order by " + FECHA_PUBLICACION + " limit ? ";
//	private static final String SELECT_PUBLICACIONES_AMIGOS = "select * from " + PUBLICACIONES_TABLE
//			+ " where "+ID_USUARIO+" in (?) order by " + FECHA_PUBLICACION + " limit ? ";

	private static final String SELECT_ID_PUBLICACIONES = "select max(" + ID_PUBLICACION + ")+1 from "
			+ PUBLICACIONES_TABLE;

	private static final String DELETE_PUBLICACION = "delete from " + PUBLICACIONES_TABLE + " where " + ID_PUBLICACION+ " = ?";

	private static final String SELECT_PUBLICACIONES_USUARIO = "select * from " + PUBLICACIONES_TABLE +" where "+ID_USUARIO+" = ?"
			+ " order by " + FECHA_PUBLICACION+ " limit ? ";


	public static void insert(Publicacion publicacion) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ID_PUBLICACIONES);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			publicacion.setId(rs.getInt(1));
		} else {
			throw new SQLException("Error obtener ID");
		}
		query = MysqlCon.getQuery(INSERT_PUBLICACION);
		query.setLong(1, publicacion.getId());
		query.setString(2, publicacion.getComentario());
		query.setLong(3, publicacion.getCreador().getId());
		query.setString(4, publicacion.getTitiulo());
		query.execute();
		//MysqlCon.endConnecction();
	}

	public static void delete(long idComentario) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(DELETE_PUBLICACION);
		query.setLong(1, idComentario);
		query.execute();
		//MysqlCon.endConnecction();
	}

	public static void likeDislike(long idPublicacion,boolean like ) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(UPDATE_LIKES);
		query.setString(1, like?"+":"-");
		query.setLong(2, idPublicacion);
		query.execute();
		//MysqlCon.endConnecction();
	}

	public static List<Publicacion> selectPublicaciones(int limit)
			throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_PUBLICACIONES_GLOBAL);
		query.setInt(1, limit);
		ResultSet rs = query.executeQuery();
		List<Publicacion> publicaciones = new ArrayList<>();
		while (rs.next()) {
			Usuario usuario = UsuariosDAO.selectUsuarioNoMontos(new Usuario(rs.getLong(ID_USUARIO)));
			Publicacion publicacion = new Publicacion(rs.getString(COMENTARIO), usuario,rs.getString(TITULO));
			publicacion.setLikeDislike(rs.getInt(LIKE_DISLIKE));
			publicacion.setFechaPublicaion(new Date(rs.getTimestamp(FECHA_PUBLICACION).getTime()));
			publicacion.setId(rs.getInt(ID_PUBLICACION));
			publicacion.setOpiniones(OpinionDAO.selectComentariosPublicacion(publicacion.getId()));
			publicaciones.add(publicacion);
		}
		//MysqlCon.endConnecction();
		return publicaciones;
	}

	public static List<Publicacion> selectPublicacionesByUsuario(int limit ,long idCreador) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_PUBLICACIONES_USUARIO);
		query.setLong(1, idCreador);
		query.setInt(2, limit);
		ResultSet rs = query.executeQuery();
		List<Publicacion> publicaciones = new ArrayList<>();
		while (rs.next()) {
			Usuario usuario = new Usuario();
			usuario.setId(rs.getLong(ID_USUARIO));
			Publicacion publicacion = new Publicacion(rs.getString(COMENTARIO), usuario,rs.getString(TITULO));
			publicacion.setLikeDislike(rs.getInt(LIKE_DISLIKE));
			publicacion.setFechaPublicaion(new Date(rs.getTimestamp(FECHA_PUBLICACION).getTime()));
			publicacion.setId(rs.getInt(ID_PUBLICACION));
			publicaciones.add(publicacion);
		}
		//MysqlCon.endConnecction();
		return publicaciones;
	}

}
