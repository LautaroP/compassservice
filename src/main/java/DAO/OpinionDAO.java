package DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import clasessistema.Opinion;
import clasessistema.Publicacion;
import clasessistema.Usuario;
import utils.MysqlCon;

public class OpinionDAO {
	private static final String ID_PUBLICACION = "idPublicacion";

	private static final String ID_OPINION = "idComentario";

	private static final String COMENTARIO = "comentario";

	private static final String LIKE_DISLIKE = "likeDislike";

	private static final String OPINION_TABLE = "opiniones";

	private static final String ID_CREADOR = "idCreador";

	private static final String FECHA_PUBLICACION = "fechaPublicacion";
	
	private static final String INSERT_OPINION = "insert into " + OPINION_TABLE + " (" + ID_PUBLICACION + ","
			+ ID_OPINION + ", " + COMENTARIO + ", "+FECHA_PUBLICACION+", " + ID_CREADOR + ") values (?,?,?,CURRENT_TIMESTAMP,?)";

	private static final String UPDATE_LIKES = "udate  set " + LIKE_DISLIKE + " = "+ LIKE_DISLIKE +" ? 1 where " + ID_OPINION + " = ?";


	private static final String SELECT_ID_OPINIONES = "select max(" + ID_OPINION + ")+1 from " + OPINION_TABLE;

	private static final String DELETE_OPINION = "delete from " + OPINION_TABLE + " where " + ID_OPINION + " = ?";

	private static final String SELECT_COMENTARIOS_PUBLICACION = "select * from " + OPINION_TABLE + " where "
			+ ID_PUBLICACION + " = ? order by "+FECHA_PUBLICACION;

	private static final String SELECT_COMENTARIOS_USUARIO = "select * from " + OPINION_TABLE + " where " + ID_CREADOR
			+ " = ? order by "+FECHA_PUBLICACION;

	public static Opinion insert(Opinion opinion) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ID_OPINIONES);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			opinion.setId(rs.getInt(1));
		} else {
			throw new SQLException("Error obtener ID");
		}
		query = MysqlCon.getQuery(INSERT_OPINION);
		query.setLong(1, opinion.getPublicacion().getId());
		query.setLong(2, opinion.getId());
		query.setString(3, opinion.getComentario());
		query.setLong(4, opinion.getCreador().getId());
		query.execute();
		return opinion;
	}

	public static void delete(long idOpinion) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(DELETE_OPINION);
		query.setLong(1, idOpinion);
		query.execute();
		//MysqlCon.endConnecction();
	}

	public static void likeDislike(long idOpinion,boolean like) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(UPDATE_LIKES);
		query.setString(1, like?"+":"-");
		query.setLong(2, idOpinion);
		query.execute();
		//MysqlCon.endConnecction();
	}

	public static List<Opinion> selectComentariosPublicacion(long idPublicacion)
			throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_COMENTARIOS_PUBLICACION);
		query.setLong(1, idPublicacion);
		ResultSet rs = query.executeQuery();
		List<Opinion> opiniones = new ArrayList<>();
		while (rs.next()) {
			Usuario usuario = UsuariosDAO.selectUsuarioNoMontos(new Usuario(rs.getLong(ID_CREADOR)));
			Opinion opinion = new Opinion(rs.getString(COMENTARIO), usuario);
			opinion.setLikeDislike(rs.getInt(LIKE_DISLIKE));
			Publicacion publicacion = new Publicacion(null, null,null);
			opinion.setFechaPublicaion(new Date(rs.getTimestamp(FECHA_PUBLICACION).getTime()));
			publicacion.setId(rs.getInt(ID_PUBLICACION));
			opinion.setPublicacion(publicacion);
			opiniones.add(opinion);
		}
		//MysqlCon.endConnecction();
		return opiniones;
	}

	public static List<Opinion> selectComentariosUsuario(long idCreador) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_COMENTARIOS_USUARIO);
		query.setLong(1, idCreador);
		ResultSet rs = query.executeQuery();
		List<Opinion> opiniones = new ArrayList<>();
		while (rs.next()) {
			Usuario usuario = UsuariosDAO.selectUsuarioNoMontos(new Usuario(rs.getLong(ID_CREADOR)));

			Opinion opinion = new Opinion(rs.getString(COMENTARIO), usuario);
			opinion.setLikeDislike(rs.getInt(LIKE_DISLIKE));
			Publicacion publicacion = new Publicacion(null, null,null);
			opinion.setFechaPublicaion(new Date(rs.getTimestamp(FECHA_PUBLICACION).getTime()));
			publicacion.setId(rs.getInt(ID_PUBLICACION));
			opinion.setPublicacion(publicacion);
			opiniones.add(opinion);
		}
		//MysqlCon.endConnecction();
		return opiniones;
	}

}
