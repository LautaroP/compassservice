package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import clasessistema.Monto;
import clasessistema.Usuario;
import utils.MysqlCon;

public class MontoDAO {

	private static final String ID_MONTO = "idMonto", ID_USUARIO = "idUsuario", CANTIDAD = "cantidad",
			MONTOS_TABLE = "montos", ID_CREADOR = "idCreador";
	private static final String INVERTIDO = "invertido";
	private static final String INSERT_MONTO = "insert into " + MONTOS_TABLE + " (" + ID_MONTO + ", " + ID_USUARIO
			+ ", " + CANTIDAD + "," + ID_CREADOR + ") values (?,?,?,?)";
	private static final String UPDATE_MONTO = "update " + MONTOS_TABLE + " set " + CANTIDAD + " = ?," + ID_USUARIO
			+ " = ? where " + ID_MONTO + " = ?";
	private static final String SELECT_ID_MONTOS = "select max(" + ID_MONTO + ")+1 from " + MONTOS_TABLE;
	private static final String DELETE_MONTO = "delete from " + MONTOS_TABLE + " where " + ID_MONTO + " = ?";
	private static final String SELECT_MONTOS = "select * from " + MONTOS_TABLE + " where " + ID_CREADOR + " = ? or "
			+ ID_USUARIO + " = ?";
	private static final String SELECT_MONTO = "select * from " + MONTOS_TABLE + " where " + ID_MONTO + " = ? ";
	private static final String SET_INVERTIDO = "update " + MONTOS_TABLE + " set " + INVERTIDO + " = 1 where " + ID_MONTO + " = ?";

	public static Monto insert(Monto monto) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ID_MONTOS);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			monto.setId(rs.getInt(1));
		} else {
			throw new SQLException("Error obtener ID");
		}
		query = MysqlCon.getQuery(INSERT_MONTO);
		query.setInt(1, monto.getId());
		query.setLong(2, monto.getCreador().getId());
		query.setDouble(3, monto.getCantidad());
		query.setLong(4, monto.getCreador().getId());
		query.execute();
		UsuariosDAO.retirarFondos(monto);
		monto.setInvertido(false);
		return monto;
	}

	public static void delete(int idMonto) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(DELETE_MONTO);
		query.setInt(1, idMonto);
		query.execute();
		// MysqlCon.endConnecction();
	}

	public static void update(Monto monto, long idUsuario) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(UPDATE_MONTO);
		query.setDouble(1, monto.getCantidad());
		query.setLong(2, idUsuario);
		query.setInt(3, monto.getId());
		query.execute();
		// MysqlCon.endConnecction();
	}

	public static List<Monto> selectMotosPoseidos(long idUsuario) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_MONTOS);
		query.setLong(1, idUsuario);
		query.setLong(2, idUsuario);

		ResultSet rs = query.executeQuery();
		List<Monto> montos = new ArrayList<>();
		while (rs.next()) {
			Usuario creador = UsuariosDAO.selectUsuarioNoMontos(new Usuario(rs.getLong(ID_CREADOR)));
			Usuario posedor = UsuariosDAO.selectUsuarioNoMontos(new Usuario(rs.getLong(ID_USUARIO)));
			montos.add(new Monto(creador, rs.getDouble(CANTIDAD), posedor, rs.getInt(ID_MONTO),rs.getBoolean(INVERTIDO)));
		}
		// MysqlCon.endConnecction();
		return montos;
	}

	public static double getValorById(Integer id) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_MONTO);
		query.setInt(1, id);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			return rs.getDouble(CANTIDAD);
		}
		// MysqlCon.endConnecction();
		return 0;
	}

	public static Monto getMontoById(Integer id) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_MONTO);
		query.setInt(1, id);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			return new Monto(UsuariosDAO.selectUsuarioNoMontos(new Usuario(rs.getLong(ID_CREADOR))), rs.getDouble(CANTIDAD),
					new Usuario(rs.getLong(ID_USUARIO)), id,rs.getBoolean(INVERTIDO));
		}
		// MysqlCon.endConnecction();
		return null;
	}

	public static void setInvertido(Integer id) throws ClassNotFoundException, SQLException {
			MysqlCon.setConnection();
			PreparedStatement query = MysqlCon.getQuery(SET_INVERTIDO);
			query.setInt(1, id);
			query.execute();

	}

	public static Monto calularDevolucion(int montoId, double cantidad) throws ClassNotFoundException, SQLException {
		Monto monto = getMontoById(montoId);
		UsuariosDAO.finalizarInversion(monto.getCreador().getId(),monto.getCantidad()-cantidad);
		monto.setMonto(cantidad);
		update(monto, monto.getPosedor().getId());
		return monto;
	}

}
