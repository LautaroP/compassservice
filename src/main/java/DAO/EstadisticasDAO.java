package DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import clasessistema.Estadistica;
import clasessistema.Monto;
import clasessistema.Simulacion;
import utils.FechaPrecio;
import utils.MysqlCon;

public class EstadisticasDAO {

	private static final String ID_ESTADISTICA = "idEstadistica", NOMBRE_ACCION_MONEDA = "nombreAccionMoneda",
			VALOR = "valor", FECHA_VALOR = "fechaValor", ESTADISTICAS_TABLE = "estadisticas",
			ESTADISTICAS_ANTERIORES_TABLE = "estadisticas_anteriores", FECHA = "fecha";

	private static final String SYMBOL = "symbol";

	private static final String INSERT_ESTADISTICAS = "insert into " + ESTADISTICAS_TABLE + " (" + ID_ESTADISTICA + ", "
			+ NOMBRE_ACCION_MONEDA + ", " + VALOR + ", " + FECHA_VALOR + "," + SYMBOL + ") values (null,?,?,?,?)";
	private static final String UPDATE_ESTADISTICAS = "update " + ESTADISTICAS_TABLE + " set " + NOMBRE_ACCION_MONEDA
			+ " = ?," + VALOR + " = ? ," + FECHA_VALOR + " = ? where " + SYMBOL + " like ?";
	private static final String SELECT_ESTADISTICAS_GLOBAL = "select * from " + ESTADISTICAS_TABLE + " order by "
			+ NOMBRE_ACCION_MONEDA + " limit ? ";

	private static final String SELECT_ESTADISTICA = "select * from " + ESTADISTICAS_TABLE + " where ";

	private static final String INSERT_ESTADISTICA_ANTERIOR = "insert into " + ESTADISTICAS_ANTERIORES_TABLE
			+ " values (?,?,?)";

	private static final String SELECT_ESTADISTICAS_ANTERIORES = "select * from " + ESTADISTICAS_ANTERIORES_TABLE
			+ " where " + SYMBOL + " like ? order by " + FECHA + " DESC ";

	public static void insert(Estadistica estadistica) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(INSERT_ESTADISTICAS);
		query.setString(1, estadistica.getNombreAccionMoneda());
		query.setDouble(2, estadistica.getPrecioActual().getPrecio());
		query.setTimestamp(3, new Timestamp(estadistica.getPrecioActual().getFecha().getTime()));
		query.setString(4, estadistica.getSymbol());
		query.execute();
		// MysqlCon.endConnecction();

	}

	private static void insertAnterior(Estadistica estadistica) throws SQLException {
		PreparedStatement query = MysqlCon.getQuery(INSERT_ESTADISTICA_ANTERIOR);
		query.setString(1, estadistica.getSymbol());
		query.setTimestamp(2, new Timestamp(estadistica.getPrecioActual().getFecha().getTime()));
		query.setDouble(3, estadistica.getPrecioActual().getPrecio());
		query.execute();
		// MysqlCon.endConnecction();

	}

	public static void update(Estadistica estadistica) throws SQLException, ClassNotFoundException {
		Estadistica estadisticaAnterior = getEstadistica(estadistica);
		MysqlCon.setConnection();
		if (estadistica.getPrecioActual().getFecha().after(estadisticaAnterior.getPrecioActual().getFecha())) {
			PreparedStatement query = MysqlCon.getQuery(UPDATE_ESTADISTICAS);
			query.setString(1, estadistica.getNombreAccionMoneda());
			query.setDouble(2, estadistica.getPrecioActual().getPrecio());
			query.setTimestamp(3, new Timestamp(estadistica.getPrecioActual().getFecha().getTime()));
			query.setString(4, estadistica.getSymbol());
			query.execute();
			try {
				insertAnterior(estadisticaAnterior);
				System.out.println(estadistica.getSymbol() + estadistica.getPrecioActual().getFecha()
						+ " estadistica actual actualizada");
			} catch (Exception e) {
				System.out.println(estadistica.getSymbol() + estadistica.getPrecioActual().getFecha()
						+ " estadistica actual repetida");
			}
		} else {
			try {
				insertAnterior(estadistica);
				System.out.println(estadistica.getSymbol() + estadistica.getPrecioActual().getFecha()
						+ " estadistica anterior agregada");

			} catch (Exception e) {
				System.out.println(estadistica.getSymbol() + estadistica.getPrecioActual().getFecha()
						+ " estadistica anterior repetida");
			}
			;
		}

	}

	public static List<Estadistica> getAllEstadisticas(int limit) throws SQLException, ClassNotFoundException {
		List<Estadistica> estadisticas = new ArrayList<>();
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ESTADISTICAS_GLOBAL);
		query.setInt(1, limit);
		ResultSet rs = MysqlCon.executeQuery(query);
		while (rs.next()) {
			Estadistica estadistica = new Estadistica();
			estadistica.setId(rs.getInt(ID_ESTADISTICA));
			estadistica.setPrecioActual(new FechaPrecio<Date, Double>(new Date(rs.getTimestamp(FECHA_VALOR).getTime()),
					rs.getDouble(VALOR)));
			estadistica.setNombreAccionMoneda(rs.getString(NOMBRE_ACCION_MONEDA));
			estadistica.setSymbol(rs.getString(SYMBOL));
			estadisticas.add(estadistica);
		}
		// MysqlCon.endConnecction();
		return estadisticas;
	}

	public static Estadistica getEstadisticasAnteriores(Estadistica estadistica)
			throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ESTADISTICAS_ANTERIORES);
		query.setString(1, estadistica.getSymbol());
		ResultSet rs = MysqlCon.executeQuery(query);
		while (rs.next()) {
			estadistica.addPrecioAnterior(new Date(rs.getTimestamp(FECHA).getTime()), rs.getDouble(VALOR));
		}
		// MysqlCon.endConnecction();
		return estadistica;
	}

	public static Estadistica getEstadistica(Estadistica estadistica) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		String statement = SELECT_ESTADISTICA;
		PreparedStatement query;
		if (estadistica.getId() != null) {
			statement += ID_ESTADISTICA + " = ?";
			query = MysqlCon.getQuery(statement);
			query.setInt(1, estadistica.getId());
		} else {
			if (estadistica.getNombreAccionMoneda() != null) {
				statement += NOMBRE_ACCION_MONEDA + " like ?";
				query = MysqlCon.getQuery(statement);
				query.setString(1, estadistica.getNombreAccionMoneda());
			} else {
				statement += SYMBOL + " like ?";
				query = MysqlCon.getQuery(statement);
				query.setString(1, estadistica.getSymbol());
			}
		}
		ResultSet rs = MysqlCon.executeQuery(query);

		if (rs.next()) {
			estadistica = new Estadistica();
			estadistica.setId(rs.getInt(ID_ESTADISTICA));
			estadistica.setPrecioActual(new FechaPrecio<Date, Double>(new Date(rs.getTimestamp(FECHA_VALOR).getTime()),
					rs.getDouble(VALOR)));
			estadistica.setNombreAccionMoneda(rs.getString(NOMBRE_ACCION_MONEDA));
			estadistica.setSymbol(rs.getString(SYMBOL));
			estadistica = getEstadisticasAnteriores(estadistica);
		}
		// MysqlCon.endConnecction();
		return estadistica;
	}

	public static Double getGanancia(Simulacion simulacion) throws ClassNotFoundException, SQLException {
		simulacion.setEstadisticas(getEstadistica(new Estadistica(simulacion.getEstadisticas().getSymbol())));
		simulacion.setMonto(MontoDAO.getMontoById(simulacion.getMonto().getId()));
		return simulacion.getGanancias();

	}

	public static Estadistica getEstadisticaActual(String symbol) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		String statement = SELECT_ESTADISTICA;
		PreparedStatement query;
		statement += SYMBOL + " like ?";
		query = MysqlCon.getQuery(statement);
		query.setString(1, symbol);
		ResultSet rs = MysqlCon.executeQuery(query);

		Estadistica estadistica = null;
		if (rs.next()) {
			estadistica = new Estadistica();
			estadistica.setId(rs.getInt(ID_ESTADISTICA));
			estadistica.setPrecioActual(new FechaPrecio<Date, Double>(new Date(rs.getTimestamp(FECHA_VALOR).getTime()),
					rs.getDouble(VALOR)));
			estadistica.setNombreAccionMoneda(rs.getString(NOMBRE_ACCION_MONEDA));
			estadistica.setSymbol(rs.getString(SYMBOL));
		}
		// MysqlCon.endConnecction();
		return estadistica;
	}

}
