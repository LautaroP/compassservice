package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import clasessistema.Estadistica;
import clasessistema.Monto;
import clasessistema.Simulacion;
import clasessistema.Usuario;
import utils.MysqlCon;

public class SimulacionDAO {

	private static final String ID_SIMULACION = "idSimulacion";
	private static final String SIMULACION_TABLE = "simulaciones";
	private static final String SYMBOL = "symbol";
	private static final String ID_CREADOR = "idCreador";
	private static final String ID_MONTO = "idMonto";
	private static final String FECHA_INICIO = "fechaInicio";
	private static final String FECHA_FIN = "fechaFin";
	private static final String PRECIO_INICIO = "precioInicio";
	private static final String INSERT_SIMULACION = "insert into " + SIMULACION_TABLE + " (" + ID_SIMULACION + ", "
			+ SYMBOL + ", " + ID_CREADOR + ", " + ID_MONTO + ", " + FECHA_INICIO + "," + PRECIO_INICIO
			+ ") values (?,?,?,?,?,?)";
	private static final String UPDATE_SIMULACION = "update " + SIMULACION_TABLE + " set " + FECHA_FIN + " = ? where "
			+ ID_SIMULACION + " = ?";
	private static final String SELECT_SIMULACIONES = "select  * from " + SIMULACION_TABLE + " where " + ID_CREADOR
			+ " = ? order by " + FECHA_INICIO + " limit ? ";
	private static final String SELECT_ID_SIMULACION = "select max(" + ID_SIMULACION + ")+1 from " + SIMULACION_TABLE;
	private static final String SELECT_SIMULACION = "select  * from " + SIMULACION_TABLE + " where " + ID_SIMULACION
			+ " = ?";

	public static void insert(Simulacion simulacion) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ID_SIMULACION);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			simulacion.setId(rs.getInt(1));
		} else {
			throw new SQLException("Error obtener ID");
		}
		query = MysqlCon.getQuery(INSERT_SIMULACION);
		query.setInt(1, simulacion.getId());
		query.setString(2, simulacion.getEstadisticas().getSymbol());
		query.setLong(3, simulacion.getCreador().getId());
		query.setInt(4, simulacion.getMonto().getId());
		query.setTimestamp(5, new Timestamp(new java.util.Date().getTime()));
		query.setDouble(6, simulacion.getPrecioInicio());
		query.execute();
		MontoDAO.setInvertido(simulacion.getMonto().getId());
		// MysqlCon.endConnecction();
	}

	public static void endSimulation(Simulacion simulacion) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		simulacion = selectSimulacionById(simulacion.getId());
		if (simulacion.getFechaFin() == null) {
			PreparedStatement query = MysqlCon.getQuery(UPDATE_SIMULACION);
			query.setTimestamp(1, new Timestamp(new java.util.Date().getTime()));
			query.setLong(2, simulacion.getId());
			query.execute();
			UsuariosDAO.finalizarInversion(simulacion.getCreador().getId(), EstadisticasDAO.getGanancia(simulacion));
			// MysqlCon.endConnecction();
		} else {
			throw new SQLException("La simulacion ya estaba cerrada");
		}
	}

	public static List<Simulacion> selectSimulaciones(Simulacion simulacion, int limit)
			throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query;
		query = MysqlCon.getQuery(SELECT_SIMULACIONES);
		query.setLong(1, simulacion.getCreador().getId());
		query.setInt(2, limit);

		ResultSet rs = query.executeQuery();
		List<Simulacion> simulaciones = new ArrayList<>();
		// MysqlCon.endConnecction();
		while (rs.next()) {
			simulacion = new Simulacion(EstadisticasDAO.getEstadisticaActual(rs.getString(SYMBOL)), new Usuario(),
					MontoDAO.getMontoById(rs.getInt(ID_MONTO)), new Date(rs.getTimestamp(FECHA_INICIO).getTime()),
					rs.getTimestamp(FECHA_FIN) == null ? null : new Date(rs.getTimestamp(FECHA_FIN).getTime()),
					rs.getDouble(PRECIO_INICIO));
			simulacion.getCreador().setId(rs.getLong(ID_CREADOR));
			simulacion.setId(rs.getInt(ID_SIMULACION));
			simulaciones.add(simulacion);
		}
		return simulaciones;
	}

	public static Simulacion selectSimulacionById(int id) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query;
		query = MysqlCon.getQuery(SELECT_SIMULACION);
		query.setLong(1, id);

		ResultSet rs = query.executeQuery();
		Simulacion simulacion = null;
		// MysqlCon.endConnecction();
		if (rs.next()) {
			simulacion = new Simulacion(new Estadistica(), new Usuario(), new Monto(null, 0),
					new Date(rs.getTimestamp(FECHA_INICIO).getTime()),
					rs.getTimestamp(FECHA_FIN) == null ? null : new Date(rs.getTimestamp(FECHA_FIN).getTime()),
					rs.getDouble(PRECIO_INICIO));
			simulacion.getEstadisticas().setSymbol(rs.getString(SYMBOL));
			simulacion.getCreador().setId(rs.getLong(ID_CREADOR));
			simulacion.getMonto().setId(rs.getInt(ID_MONTO));
			simulacion.setId(rs.getInt(ID_SIMULACION));
		}
		return simulacion;
	}

}
