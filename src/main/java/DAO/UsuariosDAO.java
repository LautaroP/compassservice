package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import clasessistema.Monto;
import clasessistema.Usuario;
import utils.MysqlCon;

public class UsuariosDAO {
	private static final String USUARIOS_TABLE = "usuarios";
	private static final String ID_USUARIO = "idUsuario";
	private static final String NOMBRE_USUARIO = "nombreUsuario";
	private static final String CONTASENIA = "contrasenia";
	private static final String FONDOS_DISPONIBLES = "fondosDisponibles";
	private static final String DESCRIPCION = "descripcion";
	private static final String MAIL = "mail";
//	private static final String ID_AMIGO = "idAmigo";
//	private static final String AMIGOS_TABLE = "amigos";
	private static final String CONFIRMADO = "confirmado";

	private static final String INSERT_USUARIO = "insert into " + USUARIOS_TABLE + " (" + ID_USUARIO + ","
			+ NOMBRE_USUARIO + "," + CONTASENIA + "," + MAIL + "," + DESCRIPCION + "," + FONDOS_DISPONIBLES
			+ ") values (?,?,?,?,?,100000)";
	private static final String UPDATE_USUARIO = "update " + USUARIOS_TABLE + " set " + NOMBRE_USUARIO + " = ?,"
			+ CONTASENIA + " = ?," + FONDOS_DISPONIBLES + " = ?," + DESCRIPCION + " = ?," + MAIL + " = ?,"+CONFIRMADO+" = ? where "
			+ ID_USUARIO + " = ?";
	private static final String SELECT_USUARIO_LOGIN = "select * from " + USUARIOS_TABLE + " where " + CONTASENIA
			+ " like ? and ";
	private static final String SELECT_ID_USUARIO = "select max(" + ID_USUARIO + ")+1 from " + USUARIOS_TABLE + "";
//	private static final String SELECT_ID_AMIGOS = "select " + ID_AMIGO + " from " + AMIGOS_TABLE + " where "
//			+ ID_USUARIO + " = ?";
	private static final String SELECT_ALL_USUARIOS= "select " + ID_USUARIO + "," + NOMBRE_USUARIO + ","
			 + FONDOS_DISPONIBLES + "," + DESCRIPCION + "," + MAIL + "," + CONFIRMADO + " from " + USUARIOS_TABLE;
	private static final String DELETE_USUARIO = "delete from " + USUARIOS_TABLE + " where " + ID_USUARIO + " = ?";
	private static final String SELECT_USUARIO = "select * from " + USUARIOS_TABLE + " where ";
	private static final String RETIRAR_FONDOS = "update " + USUARIOS_TABLE + " set " + FONDOS_DISPONIBLES + " = "
			+ FONDOS_DISPONIBLES + " - ? where " + ID_USUARIO + " = ?";
	private static final String FINALIZAR_INVERSION = "update " + USUARIOS_TABLE + " set " + FONDOS_DISPONIBLES + " = "
			+ FONDOS_DISPONIBLES + " + ? where " + ID_USUARIO + " = ?";

	public static void insert(Usuario usuario) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ID_USUARIO);
		ResultSet rs = query.executeQuery();
		if (rs.next()) {
			usuario.setId(rs.getLong(1));
		} else {
			throw new SQLException("Error obtener ID");
		}
		query = MysqlCon.getQuery(INSERT_USUARIO);
		query.setLong(1, usuario.getId());
		query.setString(2, usuario.getNombreUsuario());
		query.setString(3, usuario.getContrasenia());
		query.setString(4, usuario.getMail());
		query.setString(5, usuario.getDescripcion());
		query.execute();
		// MysqlCon.endConnecction();
	}

	public static void delete(long idUsuario) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(DELETE_USUARIO);
		query.setLong(1, idUsuario);
		query.execute();
		// MysqlCon.endConnecction();
	}

	public static void update(Usuario usuario) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(UPDATE_USUARIO);
		query.setString(1, usuario.getNombreUsuario());
		query.setString(2, usuario.getContrasenia());
		query.setDouble(3, usuario.getFondosDisponibles());
		query.setString(4, usuario.getDescripcion());
		query.setString(5, usuario.getMail());
		query.setBoolean(6, usuario.isConfirmado());
		query.setLong(7, usuario.getId());
		query.execute();
		// MysqlCon.endConnecction();
	}

	public static Usuario selectLogin(Usuario usuario) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query;
		if (usuario.getNombreUsuario() == null) {
			query = MysqlCon.getQuery(SELECT_USUARIO_LOGIN.concat(MAIL + " like ?"));
			query.setString(1, usuario.getContrasenia());
			query.setString(2, usuario.getMail());
		} else {
			query = MysqlCon.getQuery(SELECT_USUARIO_LOGIN.concat(NOMBRE_USUARIO + " like ?"));
			query.setString(1, usuario.getContrasenia());
			query.setString(2, usuario.getNombreUsuario());
		}
		ResultSet rs = query.executeQuery();
		usuario = null;
		if (rs.next()) {
			usuario = new Usuario();
			usuario.setConfirmado(rs.getBoolean(CONFIRMADO));
			usuario.setContrasenia(rs.getString(CONTASENIA));
			usuario.setDescripcion(rs.getString(DESCRIPCION));
			usuario.setFondosDisponibles(rs.getDouble(FONDOS_DISPONIBLES));
			usuario.setId(rs.getLong(ID_USUARIO));
			usuario.setMail(rs.getString(MAIL));
			usuario.setNombreUsuario(rs.getString(NOMBRE_USUARIO));
			usuario.setMontos(MontoDAO.selectMotosPoseidos(usuario.getId()));
		}
		// MysqlCon.endConnecction();
		return usuario;
	}

	public static Usuario selectUsuarioNoMontos(Usuario usuario) throws SQLException, ClassNotFoundException {
		MysqlCon.setConnection();
		PreparedStatement query;
		if (usuario.getNombreUsuario() != null) {
			query = MysqlCon.getQuery(SELECT_USUARIO.concat(MAIL + " like ?"));
			query.setString(1, usuario.getMail());
		} else {
			;
			if (usuario.getNombreUsuario()!= null) {
				query = MysqlCon.getQuery(SELECT_USUARIO.concat(NOMBRE_USUARIO + " like ?"));
				query.setString(1, usuario.getNombreUsuario());
			}else {
				query = MysqlCon.getQuery(SELECT_USUARIO.concat(ID_USUARIO + " = ?"));
				query.setLong(1, usuario.getId());
			}
		}
		ResultSet rs = query.executeQuery();
		usuario = null;
		// MysqlCon.endConnecction();
		if (rs.next()) {
			usuario = new Usuario();
			usuario.setConfirmado(rs.getBoolean(CONFIRMADO));
			usuario.setDescripcion(rs.getString(DESCRIPCION));
			usuario.setId(rs.getLong(ID_USUARIO));
			usuario.setMail(rs.getString(MAIL));
			usuario.setNombreUsuario(rs.getString(NOMBRE_USUARIO));
		}
		return usuario;
	}

	public static void retirarFondos(Monto monto) throws ClassNotFoundException, SQLException {

		try {
			MysqlCon.setConnection();

			PreparedStatement query = MysqlCon.getQuery(RETIRAR_FONDOS);
			query.setDouble(1, monto.getCantidad());

			query.setLong(2, monto.getCreador().getId());
			query.execute();
		} catch (SQLException | ClassNotFoundException e) {
			MontoDAO.delete(monto.getId());
			throw new SQLException();
		}
		// MysqlCon.endConnecction();
	}

	public static void finalizarInversion(long idUsuario, Double monto) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(FINALIZAR_INVERSION);
		query.setDouble(1, monto);
		query.setLong(2, idUsuario);
		query.execute();
		// MysqlCon.endConnecction();
	}

	public static List<Usuario> selectAll() throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ALL_USUARIOS);
		ResultSet rs = query.executeQuery();
		List<Usuario> usuarios = new ArrayList<>();
		while (rs.next()) {
			Usuario usuario = new Usuario();
			usuario.setConfirmado(rs.getBoolean(CONFIRMADO));
			usuario.setDescripcion(rs.getString(DESCRIPCION));
			usuario.setId(rs.getLong(ID_USUARIO));
			usuario.setMail(rs.getString(MAIL));
			usuario.setNombreUsuario(rs.getString(NOMBRE_USUARIO));
			usuarios.add(usuario);
		}
		return usuarios;
	}

	public static List<Usuario> selectSame(String nombreUsuario) throws ClassNotFoundException, SQLException {
		MysqlCon.setConnection();
		PreparedStatement query = MysqlCon.getQuery(SELECT_ALL_USUARIOS.concat(" where "+NOMBRE_USUARIO+" like "));
		query.setString(1, "%"+nombreUsuario+"%");
		ResultSet rs = query.executeQuery();
		List<Usuario> usuarios = new ArrayList<>();
		while (rs.next()) {
			Usuario usuario = new Usuario();
			usuario.setConfirmado(rs.getBoolean(CONFIRMADO));
			usuario.setDescripcion(rs.getString(DESCRIPCION));
			usuario.setId(rs.getLong(ID_USUARIO));
			usuario.setMail(rs.getString(MAIL));
			usuario.setNombreUsuario(rs.getString(NOMBRE_USUARIO));
		}
		return usuarios;
	}

}
