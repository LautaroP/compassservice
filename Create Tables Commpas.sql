CREATE DATABASE `compass` ;
CREATE TABLE `estadisticas` (
  `idEstadistica` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombreAccionMoneda` varchar(45) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `fechaValor` datetime NOT NULL,
  `symbol` varchar(45) NOT NULL,
  PRIMARY KEY (`idEstadistica`), 
  UNIQUE KEY `idEstadistica_UNIQUE` (`idEstadistica`) /*!80000 INVISIBLE */,
  UNIQUE KEY `estadisticascol_UNIQUE` (`symbol`)
) ENGINE=InnoDB AUTO_INCREMENT=59;



CREATE TABLE `estadisticas_anteriores` (
  `symbol` varchar(45) NOT NULL,
  `fecha` datetime NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  PRIMARY KEY (`symbol`,`fecha`),
  KEY `est_estadistica_anterior_idx` (`symbol`),
  CONSTRAINT `est_estadistica_anterior_idx` FOREIGN KEY (`symbol`) REFERENCES `estadisticas` (`symbol`)
) ENGINE=InnoDB ;

CREATE TABLE `usuarios` (
  `idUsuario` int(10) unsigned NOT NULL,
  `nombreUsuario` varchar(30) NOT NULL,
  `contrasenia` varchar(30) NOT NULL,
  `fondosDisponibles` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `descripcion` varchar(200) DEFAULT NULL,
  `mail` varchar(45) NOT NULL,
  `confirmado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `nombreUsuario_UNIQUE` (`nombreUsuario`),
  UNIQUE KEY `id_UNIQUE` (`idUsuario`),
  UNIQUE KEY `mail_UNIQUE` (`mail`)
) ENGINE=InnoDB ;

CREATE TABLE `montos` (
  `idMonto` int(10) unsigned NOT NULL,
  `idUsuario` int(10) unsigned NOT NULL,
  `cantidad` decimal(10,2) unsigned NOT NULL,
  `idCreador` int(10) unsigned NOT NULL,
  `invertido` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`idMonto`),
  KEY `idUsuario_idx` (`idUsuario`),
  KEY `creador_monto_idx` (`idCreador`),
  CONSTRAINT `creador_monto` FOREIGN KEY (`idCreador`) REFERENCES `usuarios` (`idusuario`),
  CONSTRAINT `usuario_monto` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB ;

CREATE TABLE `simulaciones` (
  `symbol` varchar(45) NOT NULL,
  `idCreador` int(10) unsigned NOT NULL,
  `idMonto` int(10) unsigned NOT NULL,
  `fechaInicio` datetime NOT NULL,
  `fechaFin` datetime DEFAULT NULL,
  `idSimulacion` int(10) NOT NULL,
  `precioInicio` decimal(10,2) unsigned NOT NULL,
  PRIMARY KEY (`idSimulacion`),
  UNIQUE KEY `idSimulacion_UNIQUE` (`idSimulacion`),
  KEY `simulacion_monto_idx` (`idMonto`),
  KEY `usuario_simulacion_idx` (`idCreador`),
  KEY `sim_esta` (`symbol`),
  CONSTRAINT `sim_esta` FOREIGN KEY (`symbol`) REFERENCES `estadisticas` (`symbol`),
  CONSTRAINT `simulacion_monto` FOREIGN KEY (`idMonto`) REFERENCES `montos` (`idmonto`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuario_simulacion` FOREIGN KEY (`idCreador`) REFERENCES `usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;

CREATE TABLE `publicaciones` (
  `idPublicaciones` int(10) unsigned NOT NULL,
  `comentario` text NOT NULL,
  `likeDislike` int(10) unsigned zerofill DEFAULT NULL,
  `fechaPublicacion` datetime NOT NULL,
  `idUsuario` int(10) unsigned NOT NULL,
  `titulo` varchar(45) NOT NULL,
  PRIMARY KEY (`idPublicaciones`),
  UNIQUE KEY `idPublicaciones_UNIQUE` (`idPublicaciones`),
  KEY `idUsuario_idx` (`idUsuario`),
  CONSTRAINT `usuario_publicacion` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB ;

CREATE TABLE `opiniones` (
  `idPublicacion` int(10) unsigned NOT NULL,
  `idCreador` int(10) unsigned NOT NULL,
  `comentario` text NOT NULL,
  `likeDislike` int(11) DEFAULT '0',
  `idComentario` int(10) unsigned NOT NULL,
  `fechaPublicacion` datetime NOT NULL,
  PRIMARY KEY (`idComentario`),
  KEY `idPublicaion_idx` (`idPublicacion`),
  KEY `idUsuario_idx` (`idCreador`),
  CONSTRAINT `opinion_publicacion` FOREIGN KEY (`idPublicacion`) REFERENCES `publicaciones` (`idpublicaciones`),
  CONSTRAINT `usuario_opinion` FOREIGN KEY (`idCreador`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB ;

